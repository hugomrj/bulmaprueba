function Usuario() {
    
    this.tipo = "usuario";  
    this.campoid=  'cod_usuario';
    this.tablacampos =  ['cod_usuario', 'nombre']; 
}
 
 
 
Usuario.prototype.table = function( obj ) {         
    
        //var url =  html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/cab.html'     
        var url =  uma.html.url.absolute()+"/app/mod/configuracion/usuario/lista.html";

        fetch(url)
          .then( response => {
            return response.text();
          })
          .then(data => { 
          
            //document.getElementById('main').innerHTML = data ;
            uma.main.innerHTML = data;

            this.lista();
                
          })
    

    
};



Usuario.prototype.lista = function( obj ) {    

    const promise = new Promise((resolve, reject) => {
        
        
        uma.spiner.inicio();
        
        var url = "http://181.94.213.56:8383/ords/hmsistems/consultas/usuarios"; 
        var xhr =  new XMLHttpRequest();                    
        var metodo = "GET";                         

        
        xhr.open( metodo.toUpperCase(),   url,  true );      
        
        
        xhr.onreadystatechange = function () {
            if (this.readyState == 4 ){

                uma.spiner.fin();

                //ajax.state = xhr.status;
                var ojson = JSON.parse( xhr.responseText ) ;     
                uma.table.json = JSON.stringify(ojson['items']) ;                

                var usuario = new Usuario();
                uma.table.ini(usuario);
                uma.table.gene();                  

                usuario.lista_acciones(usuario);

                resolve( xhr );
            }
        };
                     

        xhr.setRequestHeader('Content-Type', "application/json");   
        xhr.send( null );                       

    })

    return promise;


};


 
Usuario.prototype.lista_acciones = function( obj ) {     

 var usuario_agregar = document.getElementById( "usuario_agregar" );  
 usuario_agregar.onclick = function() {    
  
    var url =  uma.html.url.absolute()+"/app/mod/configuracion/usuario/form.html";
    fetch(url)
      .then( response => {
        return response.text();
      })
      .then(data => { 
        uma.main.innerHTML = data;
      })
      .then(data => { 
        //uma.main.innerHTML = data;


        var usuario_guardar = document.getElementById( "usuario_guardar" );  
        usuario_guardar.onclick = function() {  

          uma.msg.dom = "wrapper";                          
          uma.msg.error.mostrar("Error de acceso"); 


        }




      })

    };    


};