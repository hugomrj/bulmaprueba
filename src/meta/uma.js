var uma = {     

    app : "bulmaprueba",
    main : null,

    spiner:{
         
        /*dom: null,*/
        id:  "modal", 
        count: 0,
           
        inicio : function  ()
        {            
            if ( uma.spiner.count == 0 ){
                   
                var modal = document.createElement("div");
                modal.setAttribute("id", uma.spiner.id );
                modal.classList.add('modal');
                modal.classList.add('is-active');
    
                var modalback = document.createElement("div");
                modalback.classList.add('modal-background');
                modal.appendChild(modalback);
                
                var spin = document.createElement("div");
                spin.classList.add('spiner');
                modal.appendChild(spin);

                var lds = document.createElement("div");
                lds.classList.add('lds-dual-ring');
                spin.appendChild(lds);

                document.body.appendChild(modal);      
    /*          loader.dom = loade; */  
            }
            uma.spiner.count ++;
        },

        fin : function  ()
        {            
            if (uma.spiner.count == 1) {

                dom = document.getElementById( uma.spiner.id );
                document.body.removeChild( dom );
            }
            uma.spiner.count --;
            
        }         

    },
            
            

    msg:{

        dom: null,

        error : {        
        
            mostrar: function(mensaje) {
                
                //var UID = msg.generarUID();
                //msg.crear(UID, mensaje ).setAttribute("class", "alerta alerta-error "); ;     
                
                uma.msg.eliminar();
                uma.msg.crear(mensaje, "is-danger");
                
        
            }
        },        






        crear:  function(mensaje, clase ) {

            var msn = document.createElement("div");
            msn.setAttribute("id", "msn" );

            var block = document.createElement("div");
            block.classList.add('block');
            /*block.classList.add('m-4');*/
            msn.appendChild(block)            


            var columns = document.createElement("div");
            /*columns.classList.add('columns');            */
            block.appendChild(columns)            

            
            var col = document.createElement("div");
            col.classList.add('column');            
            col.classList.add('is-four-fifths-tablet');  
            col.classList.add('is-two-thirds-desktop');  
            col.classList.add('is-half-fullhd');              
            col.classList.add('p-0');              
            columns.appendChild(col)            


            var noti = document.createElement("div");
            noti.classList.add('notification');            
            noti.classList.add(clase);       
            noti.innerHTML = mensaje ;     
            col.appendChild(noti)             

            var del = document.createElement("button");
            del.classList.add('delete');
            del.setAttribute("id", "delmsn" );
            noti.appendChild(del);


            if (uma.msg.dom == null){
                document.body.appendChild(msn);
            }
            else{
                var dom = document.getElementById(uma.msg.dom);
                dom.appendChild(msn);
            }


            var delmsn = document.getElementById("delmsn");            
            delmsn.onclick = function(){    
                uma.msg.eliminar();
            }           

        },     
        
        eliminar:  function() {
            if(document.getElementById("msn")){
                var element = document.getElementById("msn"); 
                element.parentNode.removeChild(element);
            }            
        },        


    },


    form:{
        name: "",   
        datos: {   
        
            getjson: function( ) {    
    
                var campos = document.getElementById( uma.form.name )
                    .getElementsByTagName("input");  
                                  
                var arr = [];            
                var str = "";        
    
                for (var i=0; i< campos.length; i++) {         
    
                    // control d elementos no repetidos  // no funnciona              
                    var idx = arr.indexOf(campos[i].name);
                    
                    if (idx == -1){
                        
                        if (!(typeof campos[i].dataset.vista === "notGet" )){     
                            arr.push(campos[i].name);    
                        }     
                    }
                    else{
                        break;
                    }
                    
                    
                    var ele = campos[i];                   
              
                    var type= ele.type;
                    switch(type) {

                        case "hidden":                                                 

                            if (typeof ele.dataset.pertenece === "undefined" ){     
                                str =  str  + "";
                            }  
                            else
                            {                                
                                if (str  != ""){
                                    str =  str  + ",";
                                }                                                
                                str =   str + uma.form.datos.elemetiq(campos[i]) ;                           
                            }                            
                            break;                        


                        case "radio":                                                 
                            
                            if (str  != ""){
                                str =  str  + ",";
                            }   
                            
                            str =   str + uma.form.radios.getdatabol(ele.name );
                            
                            break;                        


                        default:
                            
                            if (typeof ele.dataset.vista === "undefined" ){    
                                if (str  != ""){
                                    str =  str  + ",";
                                }                                                                                    
                                str =   str + uma.form.datos.elemetiq(campos[i]) ;                    
                            }    
                            
                    }                       
    
                }
                
    
                
                
                
                var str2 = "";  
                var combos = document.getElementById( uma.form.name ).getElementsByTagName("select");   
                
                for (var y=0; y< combos.length; y++) {               
                    
                    if (!(combos[y].dataset.vista === "notGet" )) {     
                        if (str2  != ""){
                            str2 =  str2  + ",";
                        }                                                            
                        str2 =  str2 + uma.form.datos.elemetcombo(combos[y]) ;    
                    }                          
        
                }
    
                if (str2  != ""){
                    if (str  != ""){
                        str =  str  + "," + str2;
                    }
                    else{
                        str =  str2;
                    }
                }
    
    
    
                // textarea
                var str3 = "";  
                var areas = document.getElementById( uma.form.name ).getElementsByTagName("textarea");   
                for (var x=0; x< areas.length; x++) {                
    
                        if (str3  != ""){
                            str3 =  str3  + ",";
                        }                                                            
                        str3 =  str3 + uma.form.datos.elemenTextArea(areas[x]);                      
                }
    
                if (str3  != ""){
                    if (str  != ""){
                        str =  str  + "," + str3;
                    }
                    else{
                        str =  str3;
                    }
                }
    
                return "{" +str+ "}"  ;            
            },   
            
            
            elemen: function( ele ) {    
                
                var str = "";        
                
                str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;
                str =   str + ":";            
                
                if (ele.type == "text"  ||  ele.type == "hidden"  )
                {  
                    if (ele.className == "num"){
                        //str = str + ele.value  ;    
                        str = str + NumQP(ele.value)  ;    
                    }
                    else{
                        str = str + "\"" +ele.value+ "\"" ;
                    }                
                }
                else
                {
                    
                    if (ele.type == "password"){
                        str =   str + "\"" + ele.value+ "\"" ;
                    }
                    if (ele.type == "date"){
                    
                    
                        if (ele.value !=  "")
                        {  
                            str =   str + "\"" + ele.value+ "\"" ;
                        }
                        else {
                            str =   str + " null " ;
                        }
    
                        //str =   str + "\"" + ele.value+ "\"" ;
                    }                                
                }
                
                return str ;            
            },
                
                
            
            elemenTextArea: function( ele ) {    
                
                var str = "";        
                
                str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;
                str =   str + ":";            
                
                str = str + "\"" +ele.value+ "\"" ;
                
                return str ;            
            },
                            
                
                
                
            elemetiq: function( ele ) {                
                
    
                var str = "";              
                
                if (typeof ele.dataset.foreign === "undefined" ){             
                    
                    str =   str + uma.form.datos.elemen(ele).toString();
    
                }
                else
                {
                  
                    
                    if (typeof ele.dataset.json === "undefined" ){             
                        var e = ele.dataset.foreign.toString().toLocaleLowerCase();
                    }                
                    else{
                        var e = ele.dataset.json.toString().toLocaleLowerCase();
                    }
    
    
                    if (e == ele.getAttribute('name') ){                                 
                        str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;                
                        
                    }
                    else{                    
                        str =  str  + "\"" +e+ "\"" ;                                        
                    }
                                                     

                    str =   str + ":";                  
                    str =  str + "{ "+  uma.form.datos.elemen(ele) + " }" ;
                }           
                return str ;            
            },
            
            
            elemetcombo: function( ele ) {                      
                
                var str = "";              
                
                if (typeof ele.dataset.foreign === "undefined" ){         
                    str = str + " \"" +ele.name+ "\" " ;            
                }
                
                else{                
                    var e = ele.dataset.foreign.toString().toLocaleLowerCase();
                    str = str + " \"" + e + "\"" ;            
                } 
                
                str =   str + ":";            
                str =  str + "{ ";  
                str =  str + " \"" +ele.name+ "\": " ;
                    if (ele.className == "num")
                    {
                        str = str + NumQP(ele.value)  ;    
                    }
                    else
                    {
                        str = str + "\"" +ele.value+ "\"" ;
                    }             
                str =  str + "} ";
                return str ;            
            }, 
            
        },   
    },
    
    table:{

        id:  "",
        json:  "",
        tbody_id:"",
        html:"",
        linea:"",
        campos: ['uno', 'dos'],


    
        ini: function(obj) {        
        
            uma.table.id = "table_" + obj.tipo;            
            uma.table.tbody_id = "tbody_" + obj.tipo;            

            uma.table.linea = obj.campoid;
            uma.table.campos = obj.tablacampos;
            uma.table.etiquetas = obj.etiquetas;
    
        },
        


        gene: function() {
            document.getElementById( uma.table.tbody_id ).innerHTML = uma.table.get_html();  
        },
    
    
        get_html: function(  strjson  ) {

            if (strjson === undefined) {
                var oJson = JSON.parse(uma.table.json) ;       
            }
            else {
                var oJson = JSON.parse( strjson ) ;                    
            }
            
            
            uma.table.html = "";
                 
            
            for(x=0; x < oJson.length; x++) {     
        
                uma.table.html += "<tr data-linea_id=\""+
                        oJson[x][uma.table.linea]
                        +"\">";                  
    
    
                uma.table.campos.forEach(function (elemento, indice, array) {                                
    
                    //tabla.html += "<td>";     
    
                    try { 
                        uma.table.html += "<td data-title=\""+ uma.table.etiquetas[indice] +"\">";                    
                    }
                    catch (e) {
                        uma.table.html += "<td>";     
                    }                
    
                    
                    //tabla.html += "<td data-title=\""+tabla.etiquetas[indice]+"\">";                    
                    var jsonprop;
    
          
                    if ( uma.util.countChar(elemento, ".") > 0 ){
    
                        
                    eval("jsonprop = oJson[x]" + uma.table.campos_combinados(elemento) + ";"); 
                        
                    }
                    else
                    {
                        
                        try {                        
    
                            eval("jsonprop = oJson[x]."+ elemento + ";");                            
                        }
                        catch(error) {                        
                            jsonprop = "";        
                        }
                    }
    
                    uma.table.html += jsonprop;     
                    
                    uma.table.html += "</td>";
    
                });    
             
                uma.table.html += "</tr>";    
            }
    
            return uma.table.html;
        },
        
                


    },



    html:{
        url:{
            absolute: function() 
            {

                

console.log( location.host)                

console.log( location.origin)           

console.log( window.location.pathname)                




                var pathname = window.location.pathname;
                pathname = pathname.substring(1,pathname.length);
                pathname = pathname.substring(0,pathname.indexOf("/"));

console.log( pathname)            

                if (uma.app == pathname){
                    pathname = "/"+pathname;
                }
                else{
                    pathname = location.origin;
                }

console.log( pathname)                

                return pathname ;            
            },
        },
    },
    


    ui:{
        menu_lateral:{
            generar: function(json) 
            {
                console.log(json);


                var sidemenu = document.getElementById("sidemenu");
                
                var barstart = document.createElement("div"); 
                barstart.className = "navbar-start"; 
                
                var ojson = JSON.parse( json ) ;   



                // 
                var bitem = null; 
                var navdropdown = null; 



                for(x=0; x < ojson.length; x++) { 

                    var indicador = ojson[x].nivel.indexOf("-");


                    if (!(indicador > 0)){

                        bitem = document.createElement("div"); 
                        bitem.className = "navbar-item has-dropdown"; 
                        barstart.appendChild(bitem);
        
                        var blink = document.createElement("a"); 
                        blink.className = "navbar-link is-arrowless"; 
                        blink.innerHTML = ojson[x].item; 
                        bitem.appendChild(blink);   


                        

                        //console.log(ojson[x].nivel)                    
                        navdropdown = null; 
                    }
                    else{

                        if (navdropdown == null){

                            console.log("nva is null") 

                            navdropdown = document.createElement("div");                
                            navdropdown.className = "navbar-dropdown py-0 has-background-black-ter is-hidden"; 
                            
                            bitem.appendChild(navdropdown); 

                        }
                        
                        var subitem = document.createElement("a"); 
                        subitem.className = "navbar-item"; 
                        subitem.href="javascript:void(0);"
                        subitem.id = ojson[x].dom;
                        subitem.innerHTML = ojson[x].item;



                        navdropdown.appendChild(subitem); 
                        

                    }

                }
                sidemenu.appendChild(barstart);
            },
        },
    },
    


    util:{
        countChar: function(str, c) 
        {
            var result = 0, i = 0;
            for(i;i<str.length;i++)if(str[i]==c)result++;
            return result; ;            
        },

    }

}
